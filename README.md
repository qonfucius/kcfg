# kcfg

[![pipeline status](https://gitlab.com/qonfucius/kcfg/badges/develop/pipeline.svg)](https://gitlab.com/qonfucius/kcfg/commits/develop)
[![unsafe forbidden](https://img.shields.io/badge/unsafe-forbidden-success.svg)](https://github.com/rust-secure-code/safety-dance/)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](./LICENSE)
[![Crates.io](https://img.shields.io/crates/v/kcfg.svg)](https://crates.io/crates/kcfg)
[![dependency status](https://deps.rs/crate/kcfg/0.2.1/status.svg)](https://deps.rs/crate/kcfg)

Sometimes, you have to deal with many `kubeconfig` files. `kcfg` is here to help you !

## Installation

Via Cargo:

`cargo install kcfg`

## Commands

Available commands:

- `fork` Duplicates the current KUBECONFIG file and uses it as KUBECONFIG
- `completions` Generates tab-completion script for your shell
- `help` Print this message or the help of the given subcommand(s)
- `init` Initializes the environment relative to your current path
- `use` Find and use the correct KUBECONFIG

### fork

Fork duplicate the current `$KUBECONFIG` file and uses it as `$KUBECONFIG`. 
This command will print the command to use to export the new file as the new `$KUBECONFIG` environment variable.

#### Flags

1. `-o` `--overwrite`
    The forked configuration will not use any timestamp in its file name, meaning it will overwrite previous forks of the configuration.
    
2. `-n` `--no-export`
    The program will not try to set the forked file path will as the new `$KUBECONFIG` environment variable.

#### Options

1. `-p` `--target-path`
    Target directory for the forked configuration.

### Init

Initializes the environment relative to your current path. This command will print the initialization script on the standard output.

#### Argument

The commands takes a value between:
- `bash`: the program will output a bash initialization script
- `zsh`: the program will output a zsh initialization script
- `fish`: the program will output a fish initialization script

#### Options

1. `-p` `--path`
Defines a custom initialization path. Otherwise the current path will be used.

2. `-f` `--full`
Print for shell double evaluation.

### Use

Parses recursively the kubeconfig directory (`~/.kube` by default) and retrieves the configuration file matching the parameters.
The program will output the command to export the found file as the new `KUBECONFIG` environment variable.

### Arguments

The command takes filter arguments to find the correct file.a

### Options

1. `-d` `--directory`

Allows to specify a custom kubeconfig directory instead of the default `~/.kube`

### Completion scripts (inspired by `rustup` documentation)

Command: `kcfg completions`

Enable tab completion for Bash, Fish, Zsh, or PowerShell. The script is output on `stdout`, allowing one to re-direct the
output to the file of their choosing. Where you place the file will depend on which shell, and which operating system you are
using. Your particular configuration may also determine where these scripts need to be placed.

Here are some common set-ups for the three supported shells under Unix and similar operating systems (such as GNU/Linux).

#### BASH:

Completion files are commonly stored in `/etc/bash_completion.d/` for system-wide commands, but can be stored in
`~/.local/share/bash-completion/completions` for user-specific commands.
    
Run the command:

> $ mkdir -p ~/.local/share/bash-completion/completions
> 
> $ kcfg completions bash >> ~/.local/share/bash-completion/completions/kcfg

This installs the completion script. You may have to log out and log back in to your shell session for the changes to take effect.

#### BASH (macOS/Homebrew):

Homebrew stores bash completion files within the Homebrew directory.
With the `bash-completion` brew formula installed, run the command:

> $ mkdir -p $(brew --prefix)/etc/bash_completion.d
> 
> $ kcfg completions bash > $(brew --prefix)/etc/bash_completion.d/kcfg.bash-completion

#### FISH:

Fish completion files are commonly stored in `$HOME/.config/fish/completions`. 

Run the command:

> $ mkdir -p ~/.config/fish/completions
> 
> $ kcfg completions fish > ~/.config/fish/completions/kcfg.fish

This installs the completion script. You may have to log out and log back in to your shell session for the changes to take effect.

#### ZSH:

ZSH completions are commonly stored in any directory listed in your `$fpath` variable. To use these completions, you must either
add the generated script to one of those directories, or add your own to this list.

Adding a custom directory is often the safest bet if you are unsure of which directory to use. First create the directory;
for this example we'll create a hidden directory inside our `$HOME`directory:

> $ mkdir ~/.zfunc

Then add the following lines to your `.zshrc` just before `compinit`:

> fpath+=~/.zfunc

Now you can install the completions script using the following command:

> $ kcfg completions zsh > ~/.zfunc/_kcfg

You must then either log out and log back in, or simply run

> $ exec zsh

for the new completions to take effect.

#### CUSTOM LOCATIONS:

Alternatively, you could save these files to the place of your choosing, such as a custom directory inside your `$HOME`.
Doing so will require you to add the proper directives, such as `source`ing inside your login script. Consult your shells documentation for
how to add such directives.

#### POWERSHELL:

The powershell completion scripts require PowerShell v5.0+ (which comes with Windows 10, but can be downloaded separately for Windows 7 or 8.1).

First, check if a profile has already been set

> PS C:\> Test-Path $profile

If the above command returns `False` run the following

> PS C:\> New-Item -path $profile -type file -force

Now open the file provided by `$profile` (if you used the `New-Item` command it will be `${env:USERPROFILE}\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1`

Next, we either save the completions file into our profile, or into a separate file and source it inside our profile.
To save the completions into our profile simply use

> PS C:\> kcfg completions powershell >> ${env:USERPROFILE}\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1
