# CHANGELOG

## 0.2.1
* The shell type `fish` is now available for init command

## 0.2.0
* Rust update to version 1.65
* Clap update
* Usage of clap_complete instead of deprecated clap_generate
* The `init` command can't be called without shell type argument anymore
* Fixed `init` command
* The `init` command can be called with --full option for double eval

## 0.1.2

* Fixed `init` command

## 0.1.1

* Bumped `clap` version
* README improvements
* Fixed `TODO` on app description

## 0.1.0

First version