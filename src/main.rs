#![warn(missing_docs)]
#![forbid(unsafe_code)]
#![warn(
    clippy::all,
    clippy::correctness,
    clippy::suspicious,
    clippy::style,
    clippy::complexity,
    clippy::perf,
    clippy::nursery,
    clippy::cargo,
    nonstandard_style
)]

//! kcfg

use crate::app::{Command, KcfgApp};
use crate::commands::{command_fork, command_init, command_use};
use clap::Parser;
use std::process::exit;

mod app;
mod commands;
mod common;
mod error;

/// `KUBECONFIG` env var key
pub const KUBECONFIG: &str = "KUBECONFIG";

fn main() {
    let app: KcfgApp = KcfgApp::parse();

    let res = match app.command {
        Command::Init(params) => command_init::init(params),
        Command::Use(params) => command_use::router(params),
        Command::Fork(params) => command_fork::fork(params),
        Command::Completions(completion) => {
            completion.generate();
            Ok(String::new())
        }
    };
    match res {
        Ok(out) => println!("{}", out),
        Err(e) => {
            eprintln!("Error: {}", e);
            exit(e.exit_code())
        }
    }
}
