use crate::error::KcfgError;
use std::path::Path;

/// Checks that `path_str` exists and is a directory.
pub fn check_directory_path(path_str: &str) -> Result<(), KcfgError> {
    let path = Path::new(path_str);
    if !path.exists() {
        return Err(KcfgError::PathDoesNotExist(path_str.to_string()));
    }
    if !path.is_dir() {
        return Err(KcfgError::WrongDirectory(path_str.to_string()));
    }
    Ok(())
}

pub fn check_file_path(path_str: &str) -> Result<(), KcfgError> {
    let path = Path::new(path_str);
    if !path.exists() {
        return Err(KcfgError::PathDoesNotExist(path_str.to_string()));
    }
    if !path.is_file() {
        return Err(KcfgError::WrongFile(path_str.to_string()));
    }
    Ok(())
}
