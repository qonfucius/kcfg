use exitcode::ExitCode;
use std::ffi::OsString;
use std::path::PathBuf;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum KcfgError {
    #[error("{0} is not a valid path")]
    InvalidPath(PathBuf),
    #[error("{0} should be a directory")]
    WrongDirectory(String),
    #[error("{0} should be a file")]
    WrongFile(String),
    #[error("Path `{0}` does not exist")]
    PathDoesNotExist(String),
    #[error("No file name in {0}")]
    FileNameDoesNotExist(String),
    #[error("The command requires a shell type argument (bash or zsh)")]
    MissingShellType,
    #[error("{} Is not valide UTF 8", .0.to_string_lossy())]
    PathNotUnicode(OsString),
    #[error("I/O Error: {0}")]
    IOError(
        #[from]
        #[source]
        std::io::Error,
    ),
    #[error("{var} environment variable error: {source}")]
    EnvVarError {
        var: String,
        #[source]
        source: std::env::VarError,
    },
    #[error("Could not find a config using {0:?} as input")]
    ConfigNotFound(Vec<String>),
    #[error("This command requires input values")]
    MissingInput,
}

impl KcfgError {
    pub const fn exit_code(&self) -> ExitCode {
        match self {
            Self::InvalidPath(_) | Self::EnvVarError { .. } => exitcode::DATAERR,
            Self::IOError(_) => exitcode::IOERR,
            _ => exitcode::USAGE,
        }
    }
}
