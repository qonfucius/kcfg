use crate::app::KcfgApp;
use clap::{Command, CommandFactory, Parser, ValueEnum};
use clap_complete::shells::{Bash, Elvish, Fish, PowerShell, Zsh};
use clap_complete::{generate, Generator};

#[derive(Clone, Parser, Debug)]
pub struct CompletionOptions {
    /// target shell type
    #[clap(value_enum, default_value = "bash")]
    pub shell_type: ShellType,
}
#[derive(Parser, ValueEnum, Debug, Clone)]
pub enum ShellType {
    Bash,
    Elvish,
    Fish,
    #[clap(name = "powershell")]
    PowerShell,
    Zsh,
}

fn print_completions<G: Generator>(generator: G, app: &mut Command) {
    let name = app.get_name().to_string();
    generate(generator, app, name, &mut std::io::stdout());
}

impl CompletionOptions {
    pub fn generate(&self) {
        let mut app = KcfgApp::command();
        match self.shell_type {
            ShellType::Bash => print_completions(Bash, &mut app),
            ShellType::Elvish => print_completions(Elvish, &mut app),
            ShellType::Fish => print_completions(Fish, &mut app),
            ShellType::PowerShell => print_completions(PowerShell, &mut app),
            ShellType::Zsh => print_completions(Zsh, &mut app),
        };
    }
}
