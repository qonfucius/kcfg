use crate::commands::{command_completions, command_fork, command_init, command_use};
use clap::{Parser, Subcommand};

const VERSION: &str = env!("CARGO_PKG_VERSION");

#[derive(Parser)]
#[clap(
    version = VERSION,
    author = "Qonfucius team <contact+kcfg@qonfucius.team>",
    about = "Sometimes, you have to deal with many kubeconfig files. kcfg is here to help you !"
)]
/// Declares every available command and option of the program.
pub struct KcfgApp {
    #[command(subcommand)]
    pub command: Command,
}

#[derive(Debug, Clone, Subcommand)]
pub enum Command {
    /// Prints the kcfg shell initialization code
    Init(command_init::InitOptions),
    /// Find and use the correct KUBECONFIG
    Use(command_use::UseOptions),
    /// Duplicates the current KUBECONFIG file and uses it as KUBECONFIG
    Fork(command_fork::ForkOption),
    /// Generates tab-completion script for your shell
    Completions(command_completions::CompletionOptions),
}
